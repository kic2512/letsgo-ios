//
//  MapViewController.swift
//  LetsGo
//
//  Created by Саркис on 4/15/17.
//  Copyright © 2017 letsgo. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import Alamofire
import SwiftyButton

class AbstractViewController: UIViewController {
    
    var navView: UIView!
    var pageTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addNavigationBar()
        self.addNavigation()
    }
    
    override func loadView() {
        
    }
    
    
    func addNavigationBar() {
        let nav = UIView(frame: CGRect(x:0, y:0, width: UIScreen.main.bounds.size.width, height: 60))
        nav.backgroundColor = UIColor(hexString: "0f6b9f")
        let menu  = UIButton(frame: CGRect(x: 15, y: 25, width: 25, height: 25))
        menu.setImage(UIImage(named: "menu.png"), for: UIControlState.normal)
        menu.addTarget(self, action: #selector(self.toggleNav(_:)), for: .touchUpInside)
        nav.addSubview(menu)
        
        let text = UITextView(frame: CGRect(x: 60, y: 20, width: UIScreen.main.bounds.size.width, height: 30));
        text.text = self.pageTitle
        text.font = UIFont(descriptor: UIFontDescriptor.init(), size: CGFloat(17))
        text.backgroundColor = UIColor(hexString: "0f6b9f")
        text.textColor = UIColor.white
        text.isSelectable = false
        nav.addSubview(text)
        
        self.view.addSubview(nav)
    }
    
    func toggleNav(_ sender: AnyObject?) {
        if self.navView.frame.origin.x < 0 {
            UIView.animate(withDuration: 0.5) {
                self.navView.frame.origin.x += 200
            }
        } else {
            UIView.animate(withDuration: 0.5) {
                self.navView.frame.origin.x -= 200
            }
        }
    }
    
    func toMap(_ sender: AnyObject?) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        view.window!.layer.add(transition, forKey: kCATransition)
        //let controller = CreateViewController()
        performSegue(withIdentifier: "toMap", sender: self)
        //present(controller, animated: true, completion: nil)
        
        //        notificationManager.notificationsPosition = LNRNotificationPosition.top
        //        notificationManager.notificationsBackgroundColor = UIColor.white
        //        notificationManager.notificationsTitleTextColor = UIColor.black
        //        notificationManager.notificationsBodyTextColor = UIColor.darkGray
        //        notificationManager.notificationsSeperatorColor = UIColor.gray
        //
        //
        //        notificationManager.showNotification(notification: LNRNotification(title: "Событие: Бухач", body: "У вас новое уведомление", duration: LNRNotificationDuration.default.rawValue, onTap: { () -> Void in
        //            print("Notification Dismissed")
        //        }, onTimeout: { () -> Void in
        //            print("Notification Timed Out")
        //        }))
    }
    
    func toProfile(_ sender: AnyObject?) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        view.window!.layer.add(transition, forKey: kCATransition)
        performSegue(withIdentifier: "ProfileViewSegue", sender: self)
    }
    
    func addNavigation() {
        self.navView = UIView(frame: CGRect(x: -200, y: 60, width: 200, height: UIScreen.main.bounds.size.height-60))
        self.navView.backgroundColor = UIColor(hexString: "0f6b9f")
        
        let headers: HTTPHeaders = ["Authorization": "Token " + GlobalData.token!]
        let url = GlobalData.user_url
        let avatarView = UIImageView(frame: CGRect(x: 10, y: 10, width: 80, height: 80))
        avatarView.layer.cornerRadius = 40
        avatarView.clipsToBounds = true
        
        let name = UITextView(frame: CGRect(x: 100, y: 10, width: 100, height: 60))
        name.isSelectable = false
        name.backgroundColor = UIColor(hexString: "0f6b9f")
        name.textColor = UIColor.white
        name.font = UIFont.boldSystemFont(ofSize: 15)
        
        Alamofire.request(url!, headers: headers).responseJSON {
            response in
            if let JSON = response.result.value {
                if let items = JSON as? [String: AnyObject] {
                    if let avatar = items["avatar"] as? String {
                        let url = URL(string: avatar)
                        avatarView.af_setImage(withURL: url!)
                    }
                    
                    if let firstName = items["first_name"] as? String {
                        name.text = firstName
                    }
                }
            }
        }
        
        let makeButton = UIButton(frame: CGRect(x: 30, y: 100, width: 140, height: 30))
        makeButton.backgroundColor = UIColor(hexString: "06c7d5")
        makeButton.setTitle("+ событие", for: .normal)
        makeButton.titleLabel?.textColor = .white
        makeButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        makeButton.layer.cornerRadius = 5
        
        self.navView.addSubview(avatarView)
        self.navView.addSubview(name)
        self.navView.addSubview(makeButton)
        
        let menuList = [
            MenuItem(name: "Мой профиль", action: #selector(self.toProfile(_:))),
            MenuItem(name: "Мои события", action: #selector(self.toMap(_:))),
            MenuItem(name: "Мои заявки", action: #selector(self.toMap(_:))),
            MenuItem(name: "Сообщения", action: #selector(self.toMap(_:)))
        ]
        
        var menuPos = 160
        for item in menuList {
            let menuButton = UIButton(frame: CGRect(x: 30, y: menuPos, width: 140, height: 20))
            menuButton.setTitle(item.name, for: .normal)
            menuButton.addTarget(self, action: item.action!, for: .touchUpInside)
            menuButton.contentHorizontalAlignment = .left
            
            self.navView.addSubview(menuButton)
            
            menuPos += 30
        }
        
        self.view.addSubview(self.navView)
    }
}
