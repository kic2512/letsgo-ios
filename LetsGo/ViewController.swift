//
//  ViewController.swift
//  LetsGo
//
//  Created by Саркис on 4/15/17.
//  Copyright © 2017 letsgo. All rights reserved.
//

import UIKit
import SwiftyVK
import SwiftyButton
import SwiftHEXColors
import Alamofire
import ImageSlideshow

class ViewController: UIViewController, VKDelegate {

    var vkButton: PressableButton?
    var currentAppId: String = "5945764"
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let defaults = UserDefaults.standard
        
        if !defaults.bool(forKey: keys.welcomeIsLooked) {
            let slider: ImageSlideshow = ImageSlideshow(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
            slider.setImageInputs([
                ImageSource(image: UIImage(named: "Slide1")!),
                ImageSource(image: UIImage(named: "Slide2")!),
                ImageSource(image: UIImage(named: "Slide3")!)
            ])
            slider.circular = false
    
            self.addAuthButton(view: slider)
            
            self.view.addSubview(slider)
            defaults.set(true, forKey: keys.welcomeIsLooked)
        } else {
            if let token = defaults.string(forKey: keys.token), let href = defaults.string(forKey: keys.href){
                GlobalData.token = token
                GlobalData.user_url = href
                performSegue(withIdentifier: "toMap", sender: self)
            } else {
                VK.configure(withAppId: self.currentAppId, delegate: self)
                self.auth()
            }
        }
        
    }
    
    func addAuthButton(view: UIView) {
        vkButton = PressableButton()
        vkButton?.colors = .init(button: UIColor(hexString: "2980b9")!, shadow: UIColor(hexString: "2c3e50")!)
        vkButton?.frame = CGRect(x: 0, y: UIScreen.main.bounds.height-80, width: UIScreen.main.bounds.width, height: 80)
        vkButton?.shadowHeight = 5
        vkButton?.cornerRadius = 5
        vkButton?.layer.borderWidth = 3
        vkButton?.layer.cornerRadius = 5
        vkButton?.layer.borderColor = UIColor.white.cgColor
        
        vkButton?.setTitle("Войти через VK", for: .normal)
        vkButton?.titleLabel?.textColor = .white
        vkButton?.titleLabel?.font.withSize(CGFloat(20))
        vkButton?.addTarget(self, action: #selector(ViewController.vkClick(_:)), for: .touchUpInside)
        view.addSubview(vkButton!)
    }
    
    func auth() {
        let defaults = UserDefaults.standard
        
        if (VK.state == VK.States.authorized) {
            if let token = defaults.string(forKey: keys.token), let href = defaults.string(forKey: keys.href){
                GlobalData.token = token
                GlobalData.user_url = href
                performSegue(withIdentifier: "toMap", sender: self)
            }
        } else {
            self.addAuthButton(view: self.view)
        }
    }
    
    func vkClick(_ sender: PressableButton) {
        VK.configure(withAppId: self.currentAppId, delegate: self)
        VK.logIn()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func vkWillAuthorize() -> Set<VK.Scope> {
        //Called when SwiftyVK need authorization permissions.
        
        var set = Set<VK.Scope>()
        set.insert(VK.Scope.pages)
        return set//an set of application permissions
    }
    
    func vkDidAuthorizeWith(parameters: Dictionary<String, String>) {
        //Called when the user is log in.
        //Here you can start to send requests to the API.
        
        //Init
        var preparedReq = VK.API.Users.get()
        
        //Add parameters
        preparedReq.add(parameters: [VK.Arg.userId : parameters["user_id"]!])
        
        
        //Send
        let sendedReq = preparedReq.send(
            onSuccess: { response in
                let url = GlobalData.host! + "/api-token-auth/"
                let parameters: Parameters = [
                    "social_slug": "vk",
                    "external_id": parameters["user_id"],
                    "token": parameters["access_token"],
                    "first_name": response[0, "first_name"].stringValue
                ]
                let headers: HTTPHeaders = [:]
                
                Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    if let result = response.result.value as? [String:String] {
                        
                        let defaults = UserDefaults.standard
                        defaults.set(result["token"]! as String, forKey: keys.token)
                        defaults.set(result["href"]! as String, forKey: keys.href)
                        
                        GlobalData.token = result["token"]! as String
                        GlobalData.user_url = result["href"]! as String
                        
                        self.performSegue(withIdentifier: "toMap", sender: self)
                    }
                }
                
        },
            onError: {error in print(error)}
        )
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        //if let destinationVC = segue.destination as? MapViewController {
            //destinationVC.user = self.user
        }
    }
    
    func vkAutorizationFailedWith(error: AuthError) {
        //Called when SwiftyVK could not authorize. To let the application know that something went wrong.
    }
    
    func vkDidUnauthorize() {
        //Called when user is log out.
    }
    
    func vkShouldUseTokenPath() -> String? {
        // ---DEPRECATED. TOKEN NOW STORED IN KEYCHAIN---
        //Called when SwiftyVK need know where a token is located.
        return "/" //Path to save/read token or nil if should save token to UserDefaults
    }
    
    func vkWillPresentView() -> UIViewController {
        return self //UIViewController that should present authorization view controller
    }
}
