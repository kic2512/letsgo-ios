//
//  CreateViewController.swift
//  letsgo
//
//  Created by Саркис on 2/23/17.
//  Copyright © 2017 bauman. All rights reserved.
//

import Foundation
import UIKit
import Eureka
import CoreLocation
import Alamofire
import Toucan
import SCLAlertView
import SwiftHEXColors

class EditProfileViewController: FormViewController {
    var navigationOptionsBackup : RowNavigationOptions?
    var user: UserEdit?
    
    var removeCount: NSInteger = 0
    var sendCount: NSInteger = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var rules = RuleSet<String>()
        rules.add(rule: RuleRequired())
        
        self.tableView?.frame.size.height = (self.tableView?.frame.size.height)! * 0.75
        self.tableView?.backgroundColor = UIColor(hexString: "ffffff")
        
        let layer = CALayer()
        layer.frame = CGRect(x: 0, y: (self.tableView?.frame.size.height)!, width: (self.tableView?.frame.size.width)!, height: 3)
        layer.backgroundColor = UIColor(hexString: "c7c7cc")?.cgColor
        
        self.view.layer.addSublayer(layer)
        self.view.backgroundColor = UIColor(hexString: "ffffff")
        
        form +++ Section("Редактирование профиля")
            <<< TextRow(){ row in
                row.title = "Ник"
                row.tag = "username"
                row.placeholder = "Ник"
                //row.value = user?.firstName
                row.add(ruleSet: rules)
                row.validationOptions = .validatesOnChangeAfterBlurred
            }
            <<< TextRow(){ row in
                row.title = "Имя"
                row.tag = "first_name"
                row.placeholder = "Имя"
                //row.value = user?.firstName
                row.add(ruleSet: rules)
                row.validationOptions = .validatesOnChangeAfterBlurred
            }
            <<< TextRow(){ row in
                row.title = "Обо мне"
                row.tag = "about"
                row.placeholder = "Расскажите немного о себе..."
                //row.value = user?.about
                row.add(ruleSet: rules)
                row.validationOptions = .validatesOnChangeAfterBlurred
        } +++ MultivaluedSection(multivaluedOptions: [.Reorder, .Insert, .Delete],
                               header: "Multivalued TextField",
                               footer: ".Insert adds a 'Add Item' (Add New Tag) button row as last cell.") {
                                $0.addButtonProvider = { section in
                                    return ButtonRow(){
                                        $0.title = "Add New Tag"
                                    }
                                }
                                $0.multivaluedRowToInsertAt = { index in
                                    return ImageRow() {
                                        $0.placeholder = "Tag Name"
                                    }
                                }
                                $0 <<< ImageRow() {
                                    $0.placeholder = "Tag Name"
                                }
        }
//        
//        
//        let imageLeft = UIImage(named: "backChevron")
//        let imageRight = UIImage(named: "success")
//        
//        let imageLeftView = UIButton(type: UIButtonType.custom)
//        imageLeftView.setImage(imageLeft?.withRenderingMode(.alwaysOriginal), for: UIControlState.normal)
//        imageLeftView.frame = CGRect(x: 30, y: (self.view.frame.height - 130), width: 80, height: 80)
//        imageLeftView.addTarget(self, action: #selector(EditProfileController.goBack), for: UIControlEvents.touchUpInside)
//        self.view.addSubview(imageLeftView)
//        
//        let imageRightView = UIButton(type: UIButtonType.custom)
//        imageRightView.setImage(imageRight?.withRenderingMode(.alwaysOriginal), for: UIControlState.normal)
//        imageRightView.addTarget(self, action: #selector(EditProfileController.createClicked), for: UIControlEvents.touchUpInside)
//        imageRightView.frame = CGRect(x: self.view.frame.width - 110, y: (self.view.frame.height - 130), width: 80, height: 80)
//        self.view.addSubview(imageRightView)
        
    }
    
    func goBack() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionFade
        transition.subtype = kCATransitionFromLeft
        view.window!.layer.add(transition, forKey: kCATransition)
        //print(self.navigationController)
        _ = self.navigationController?.popViewController(animated: true)
        
        //performSegue(withIdentifier: "FromEditToProfileSegue", sender: self)
    }
    
    func createClicked() {
        let dict = form.values(includeHidden: true)
        
        let username = dict["username"] as? String
        let firstName = dict["first_name"] as? String
        let about = dict["about"] as? String
        var imageSend = [UIImage]()
        
        while self.sendCount >= 0 {
            print("photo\(self.sendCount)")
            if let image = dict["photo\(self.sendCount)"] as? UIImage {
                imageSend.append(image)
            }
            
            self.sendCount = self.sendCount - 1
        }
        
        debugPrint(imageSend)
        
        self.createEvent(username: username!, firstName: firstName!, about: about!, images: imageSend)
    }
    
    func createEvent(username: String, firstName: String, about: String, images: [UIImage]) {
        let parameters: Parameters = [
            "username": username,
            "first_name": firstName,
            "about": about
        ]
        
        var headers: HTTPHeaders = [
            "Authorization": "Token " + UserData.token!
        ]
        
        for image in images {
            
            var size = 0
            
            if image.size.width > image.size.height {
                size = Int(image.size.height)
            } else {
                size = Int(image.size.width)
            }
            
            let image = Toucan(image: image).resize(CGSize(width: size, height: size), fitMode: Toucan.Resize.FitMode.crop).image
            
            let imageData = UIImageJPEGRepresentation(image, 0.5)
            Alamofire.upload(imageData!, to: UserData.host! + "/upload-photo/name.jpg", method: .put, headers: headers).response { response in
                print(response)
            }
        }
        
        headers = [
            "Authorization": "Token " + UserData.token!,
            "Accept": "application/json"
        ]
        
        
        // All three of these calls are equivalent
        let path = "/user-detail/1/"
        Alamofire.request(UserData.host! + path, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { responce in
            //print(responce.response?.statusCode)
            if responce.response?.statusCode == 200 {
                
                self.user?.about = about
                self.user?.firstName = firstName
                
                //let image = Toucan(image: image).resize(size: CGSize, fitMode: Toucan.Resize.FitMode)
                
                //let alert = SCLAlertView().showInfo(
                //    "Профиль изменен",
                //    subTitle: "Изменили тебе профиль 😉",//random text
                //    closeButtonTitle: "Оки"
                //)
                
                //alert.setDismissBlock {
                self.goBack()
                //}
            } else {
                let alert = SCLAlertView().showError(
                    "Ошибка...",
                    subTitle: "Что-то пошло не так... Мы уже разбираемся",//random text
                    closeButtonTitle: "Оки 😔"
                )
                
                alert.setDismissBlock {
                    self.goBack()
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        let destinationVC = segue.destination as? ProfileViewController
        destinationVC?.user = self.user
    }
    
}
