//
//  MenuItem.swift
//  LetsGo
//
//  Created by Саркис on 4/29/17.
//  Copyright © 2017 letsgo. All rights reserved.
//

import Foundation

class MenuItem {
    var name: String = ""
    var action: Selector?
    
    init(name: String, action: Selector) {
        self.name = name
        self.action = action
    }
}
