//
//  MapViewController.swift
//  LetsGo
//
//  Created by Саркис on 4/15/17.
//  Copyright © 2017 letsgo. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import Alamofire
import SwiftyButton

class MapViewController: AbstractViewController, GMSMapViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        self.pageTitle = "Актуальные события"
        let camera = GMSCameraPosition.camera(withLatitude: 55.7495,longitude:37.6131, zoom:11)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera:camera)
        
        self.loadMarkers(mapView: mapView)
        
        mapView.delegate = self
        self.view = mapView
        
        self.addCreateButton()
        self.addSearchButton()
        self.addNavButton()
    }
    
    func loadMarkers(mapView: GMSMapView) {
        let headers: HTTPHeaders = ["Authorization": "Token " + GlobalData.token!]
        let url = GlobalData.host! + "/meetings-list/"
        
        Alamofire.request(url, headers: headers).responseJSON {
            response in
            if let JSON = response.result.value {
                let items = JSON as? [[String: AnyObject]]
                
                for item in items! {
                    if let coor = item["coordinates"] as? [String: AnyObject] {
                        if let lng = coor["lng"], let ltd = coor["lat"] {
                            let marker = GMSMarker()
                            marker.position = CLLocationCoordinate2DMake(ltd.doubleValue, lng.doubleValue)
                            //marker.appearAnimation = kGMSMarkerAnimationPop
                            marker.snippet = item["title"] as? String
                            //marker.icon = UIImage(named: "house")
                            let imageView = UIImageView(frame: CGRect(x: 0, y:0, width: 50, height: 50))
                            
                            if let imageUrl = item["owner"]?["avatar"] as? String, imageUrl != "" {
                                let url = URL(string: imageUrl)
                                imageView.af_setImage(withURL: url!)
                            } else {
                                imageView.backgroundColor = UIColor.gray
                            }
                        
                            imageView.layer.cornerRadius = 25
                            imageView.layer.masksToBounds = true
                            imageView.layer.borderWidth = 0
                            
                            if  let id = item["id"] as? Int,
                                let title = item["title"] as? String,
                                let latit = ltd as? Double,
                                let long = lng as? Double,
                                let photos = item["owner"]!["photos"]! as? [AnyObject],
                                let desc = item["description"] as? String
                            {
                                marker.iconView = imageView
                                marker.userData = EventData(eventId: id, name: title, ltd: latit, lng: long, photos: photos, desc: desc)
                            }
                            
                            marker.map = mapView
                        }
                    }
                }
            }
        }
    }
    
    func addCreateButton() {
        let playButton  = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width-70, y: UIScreen.main.bounds.size.height - 70, width: 60, height: 60))
        playButton.setImage(UIImage(named: "plus.png"), for: UIControlState.normal)
        playButton.addTarget(self, action: #selector(self.toMap(_:)), for: .touchUpInside)
        
        self.view.addSubview(playButton)
    }
    
    func addSearchButton() {
        let playButton  = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width-70, y: UIScreen.main.bounds.size.height/2 - 40, width: 60, height: 60))
        playButton.setImage(UIImage(named: "search.png"), for: UIControlState.normal)
        playButton.addTarget(self, action: #selector(self.toMap(_:)), for: .touchUpInside)
        
        self.view.addSubview(playButton)
    }
    
    func addNavButton() {
        let playButton  = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width-70, y: UIScreen.main.bounds.size.height/2 + 40, width: 60, height: 60))
        playButton.setImage(UIImage(named: "navigation"), for: UIControlState.normal)
        playButton.addTarget(self, action: #selector(self.toMap(_:)), for: .touchUpInside)
        
        self.view.addSubview(playButton)
    }
}
