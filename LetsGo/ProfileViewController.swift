//
//  EventViewController.swift
//  LetsGo
//
//  Created by Саркис on 4/29/17.
//  Copyright © 2017 letsgo. All rights reserved.
//

import Foundation
import UIKit
import ImageSlideshow
import AlamofireImage
import Alamofire

class ProfileViewController: AbstractViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addEditButton()
    }
    
    override func loadView() {
        self.pageTitle = "Мой профиль"
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        view.backgroundColor = UIColor.white
        self.view = view
        let slider: ImageSlideshow = ImageSlideshow(frame: CGRect(x: 0, y: 60, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width))
        var temp = [InputSource]()
        
        let headers: HTTPHeaders = ["Authorization": "Token " + GlobalData.token!]
        
        Alamofire.request(GlobalData.user_url!, headers: headers).responseJSON { response in
            if let JSON = response.result.value {
                if let item = JSON as? [String: AnyObject] {
                    let photos = item["photos"] as? [AnyObject]
                    for photo in photos! {
                        temp.append(AlamofireSource(urlString: (photo["photo"] as? String)!)!)
                    }
                    
                    slider.setImageInputs(temp)
                    view.addSubview(slider)
                    
                    if let about = item["about"] as? String {
                        let text: UITextView = UITextView(frame: CGRect(x: 0, y: UIScreen.main.bounds.width + 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - UIScreen.main.bounds.width - 100))
                        text.font = UIFont(name: "Arial", size: 16)
                        text.backgroundColor = UIColor.white
                        text.text = about
                        text.textContainerInset = UIEdgeInsetsMake(10, 0, 10, 0);
                        view.addSubview(text)
                    }
                }
            }
            
            self.view.bringSubview(toFront: self.navView)
        }
    }
    
    func addEditButton() {
        let editButton  = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width-70, y: UIScreen.main.bounds.size.height - 70, width: 60, height: 60))
        editButton.setImage(UIImage(named: "edit.png"), for: UIControlState.normal)
        editButton.addTarget(self, action: #selector(self.toEdit(_:)), for: .touchUpInside)
        
        self.view.addSubview(editButton)
    }
    
    func toEdit(_ sender: AnyObject?) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        view.window!.layer.add(transition, forKey: kCATransition)
        performSegue(withIdentifier: "toEditProfile", sender: self)
    }
}
