//
//  GlobalData.swift
//  LetsGo
//
//  Created by Саркис on 4/15/17.
//  Copyright © 2017 letsgo. All rights reserved.
//

import Foundation

struct GlobalData {
    static var token: String?
    static var host: String?
    static var user_url: String?
}

struct keys {
    static let token = "token"
    static let href = "href"
    static let welcomeIsLooked = "welcome"
}
