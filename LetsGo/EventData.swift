//
//  EventData.swift
//  letsgo
//
//  Created by Саркис on 2/21/17.
//  Copyright © 2017 bauman. All rights reserved.
//

import Foundation
import ImageSlideshow
import AlamofireImage

class EventData {
    var eventId: Int?
    var name: String?
    var ltd: Double?
    var lng: Double?
    var photos: [String]?
    var desc: String?
    
    init(eventId: Int, name: String, ltd: Double, lng: Double, photos: [AnyObject], desc: String?) {
        self.eventId = eventId
        self.name = name
        self.ltd = ltd
        self.lng = lng
        self.desc = desc
        
        self.photos = [String]()
        for item in photos {
            if let temp = item["photo"] as? String {
                self.photos?.append(temp)
            }
        }
    }
}
